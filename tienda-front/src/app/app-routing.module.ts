import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './helpers/auth.guard';
import { LoginComponent } from './login/login.component';
import { EmpleadoComponent } from './pages/empleado/empleado/empleado.component';
import { HomeComponent } from './pages/home/home/home.component';
import { ListaUsuariosComponent } from './pages/lsita-usuarios/lista-usuarios/lista-usuarios.component';
import { PersonaComponent } from './pages/persona/persona/persona.component';
import { TramiteComponent } from './pages/tramite/tramite/tramite.component';


const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'home',component:HomeComponent,canActivate:[AuthGuard]},
  {path:'persona',component:PersonaComponent,canActivate:[AuthGuard]},
  {path:'empleado',component:EmpleadoComponent,canActivate:[AuthGuard]},
  {path:'tramite',component:TramiteComponent,canActivate:[AuthGuard]},
  {path:'lista-usuarios',component:ListaUsuariosComponent,canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
