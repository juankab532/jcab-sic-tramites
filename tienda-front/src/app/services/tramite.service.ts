import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tramite } from '../models/tramite';

@Injectable({
  providedIn: 'root'
})
export class TramiteService {

  private baseUrl = 'http://localhost:8000/tramite/';  
  constructor(private http:HttpClient) { }

  getAll(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`);  
  }  

  create(datos: Tramite): Observable<object> {  
    return this.http.post(`${this.baseUrl}`, datos);  
  }  
  
  delete(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}${id}`, { responseType: 'text' });  
  }  
  
  getOne(id: number): Observable<Object> {  
    return this.http.get<Tramite>(`${this.baseUrl}${id}`);  
  }  
  
  update(id: number, params: any): Observable<Object> {  
    return this.http.put(`${this.baseUrl}${id}`, params);  
  }
}
