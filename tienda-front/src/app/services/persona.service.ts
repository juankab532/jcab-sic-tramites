import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Persona } from '../models/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  private baseUrl = 'http://localhost:8000/persona/';  
  constructor(private http:HttpClient) { }

  getAll(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`);  
  }  

  create(datos: Persona): Observable<object> {  
    return this.http.post(`${this.baseUrl}`, datos);  
  }  
  
  delete(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}${id}`, { responseType: 'text' });  
  }  
  
  getOne(id: number): Observable<Object> {  
    return this.http.get<Persona>(`${this.baseUrl}${id}`);  
  }  
  
  update(id: number, params: any): Observable<Object> {  
    return this.http.put(`${this.baseUrl}${id}`, params);  
  }
}
