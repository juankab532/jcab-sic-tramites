import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { Observable } from 'rxjs';  

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private baseUrl = 'https://gorest.co.in/public/v1/users';  
  constructor(private http:HttpClient) { }

  getUsuarioList(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`);  
  }  
}