import { Empleado } from "./empleado";
import { Persona } from "./persona";

export interface Tramite{
    id: number;
    nombre: string;
    descripcion: string;
    anio:number;
    empleado?: Empleado;
    empleadoId:number;
    persona?: Persona;
    personaId: number;
}