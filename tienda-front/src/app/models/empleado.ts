import { Persona } from "./persona";

export interface Empleado{
    id:number;
    dependencia: string;
    fechaIngreso: string;
    empleadoId:number;
    persona?:Persona;
}