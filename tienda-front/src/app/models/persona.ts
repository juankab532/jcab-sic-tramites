export interface Persona{
    id: number;
    tipoDocumento: string;
    documento:string;
    nombres:string;
    apellidos:string;
    telefono:string;
    direccion:string;
    email:string;
    password:string;
}