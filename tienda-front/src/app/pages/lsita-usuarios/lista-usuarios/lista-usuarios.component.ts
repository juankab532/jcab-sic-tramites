import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { UsuarioService } from 'src/app/services/usuario.service';



@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {

  constructor(private usuarioSerivce:UsuarioService){}
  dtOptions: DataTables.Settings = {};
  usuarios: any[]=  [];
  dtTrigger: Subject<any>= new Subject();

  ngOnInit(): void {
    this.dtOptions = {  
      pageLength: 6,  
      stateSave:true,  
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],  
      processing: true  
    };  

    this.usuarioSerivce.getUsuarioList().subscribe(response =>{  
        this.usuarios =response.data;  
        this.dtTrigger.next;
      })  
  }

}
