import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { NgForm } from '@angular/forms';
import { Persona } from 'src/app/models/persona';
import { PersonaService } from 'src/app/services/persona.service';
import { ArrayType } from '@angular/compiler';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent {
  constructor(private serivce:PersonaService){}
 
  dtOptions: DataTables.Settings = {};
  datos: any[]=  [];
  dtTrigger: Subject<any>= new Subject();
  status ="";
  idUpdating:number=NaN;
  registerUpdating:Persona ={
    id: 0,
    tipoDocumento: "",
    documento:"",
    nombres:"",
    apellidos:"",
    telefono:"",
    direccion:"",
    email:"",
    password:""
  }
  objInitial:Persona= {...this.registerUpdating};
  titulo:string="";

  ngOnInit(): void {
    this.dtOptions = {  
      pageLength: 6,  
      stateSave:true,  
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],  
      processing: true  
    };  

    this.datosTabla();

  }


  datosTabla=()=>{
    this.serivce.getAll().subscribe(response =>{  
      this.datos =response;  
      this.dtTrigger.next;
    }); 
  }
 

  modalActualizacion=(id:number)=>{
    this.status="editing";
    this.titulo="Actualización de Persona";
    this.serivce.getOne(id)
    .subscribe((response) =>{
      this.idUpdating=id;
      this.registerUpdating= { 
            id: (response as any).id,
            tipoDocumento: (response as any).tipoDocumento,
            documento:(response as any).documento,
            nombres:(response as any).nombres,
            apellidos:(response as any).apellidos,
            telefono:(response as any).telefono,
            direccion:(response as any).direccion,
            email:(response as any).email,
            password:""
        }
    });  
  }
  registrar=(data:NgForm)=>{  
    
    if(this.status=="editing"){
        this.serivce.update(this.idUpdating,this.registerUpdating)
        .subscribe((response) => { 
          this.status="confirmado";
          this.datosTabla();
        },err=>{
          this.procesarError(err.error)
        });  
    }

    if(this.status=="creating"){
      this.serivce.create(this.registerUpdating)
      .subscribe((response) => { 
        this.status="confirmado";
        this.datosTabla();
      },err=>{
        this.procesarError(err.error)
      });  
  }
  } 

  eliminar=(id:number)=>{  
    if(confirm("¿Realmente desea eliminar el registro")){
        this.serivce.delete(id)
        .subscribe((response) => { 
          this.status="";
          this.datosTabla();
        },err=>{
          this.procesarError(err.error)
        }); 
    }
  } 

  changeisUpdate=()=>{
    this.status="";
    this.idUpdating=NaN;
  }


  modalNuevo=()=>{
    this.status="creating";
    this.titulo="Crear de Persona";
    this.registerUpdating=this.objInitial;
  }


  procesarError=(error:any)=>{
    const msg = [] as string[];
    Object.values(error).forEach((val)=> {

      msg.push(val as string);
    });
    alert(msg.join("\n"))
  }
}
