import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { Tramite } from 'src/app/models/tramite';
import { Persona } from 'src/app/models/persona';
import { Empleado } from 'src/app/models/empleado';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { PersonaService } from 'src/app/services/persona.service';
import { TramiteService } from 'src/app/services/tramite.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tramite',
  templateUrl: './tramite.component.html',
  styleUrls: ['./tramite.component.css']
})
export class TramiteComponent {
  constructor(private serivce:TramiteService,private serivceP:PersonaService,private serivceE:EmpleadoService){}
 
  dtOptions: DataTables.Settings = {};
  datos: any[]=  [];
  dtTrigger: Subject<any>= new Subject();
  status ="";
  idUpdating:number=NaN;
  registerUpdating:Tramite ={
    id: NaN,
    anio: NaN,
    nombre: "",
    descripcion: "",
    empleadoId:NaN,
    personaId: NaN
  }
  objInitial:Tramite= {...this.registerUpdating};
  titulo:string="";

  personas:any;
  empleados:any;

  ngOnInit(): void {
    this.dtOptions = {  
      pageLength: 6,  
      stateSave:true,  
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],  
      processing: true  
    };  
    

    this.datosTabla();
    this.datosPersonas();
    this.datosEmpleados();

  }


  datosTabla=()=>{
    this.serivce.getAll().subscribe(response =>{  
      this.datos =response;  
      this.dtTrigger.next;
    }); 
  }

  datosPersonas=()=>{
    this.serivceP.getAll().subscribe(response =>{  
      this.personas =response;  
    }); 
  }

  datosEmpleados=()=>{
    this.serivceE.getAll().subscribe(response =>{  
      this.empleados =response;  
    }); 
  }
 

  modalActualizacion=(id:number)=>{
    this.status="editing";
    this.titulo="Actualización de trámite";
    this.serivce.getOne(id)
    .subscribe((response) =>{
      this.idUpdating=id;

      this.registerUpdating= { 
            id          : (response as any).id,
            anio          : (response as any).anio,
            nombre      : (response as any).nombre,
            descripcion : (response as any).descripcion,
            personaId   : (response as any).persona.id,
            persona     : ((response as any).persona as Persona), 
            empleadoId  : (response as any).empleado.id,
            empleado    : ((response as any).empleado as Empleado),
        }
        console.log(this.registerUpdating);
    });  
  }
  registrar=(data:NgForm)=>{  
    
    if(this.status=="editing"){
        
        this.serivce.update(this.idUpdating,this.registerUpdating)
        .subscribe((response) => { 
          this.status="confirmado";
          this.datosTabla();
        },err=>{
          this.procesarError(err.error)
        });  
    }

    if(this.status=="creating"){

      //Empleados
      this.serivceE.getOne(this.registerUpdating.empleadoId).subscribe(response =>{ 
        this.registerUpdating.empleado = (response as Empleado); 

        //Personas
        this.serivceP.getOne(this.registerUpdating.personaId).subscribe(response =>{  
                    
          this.registerUpdating.persona = (response as Persona);  

          //Trámite
          this.serivce.create(this.registerUpdating)
          .subscribe((response) => { 
            this.status="confirmado";
            this.datosTabla();
          },err=>{
            this.procesarError(err.error)
          });  

        },err=>{
          this.procesarError(err.error)
        }); 
      },err=>{
        this.procesarError(err.error)
      });  


  }
  } 

  eliminar=(id:number)=>{  
    if(confirm("¿Realmente desea eliminar el registro")){
        this.serivce.delete(id)
        .subscribe((response) => { 
          this.status="";
          this.datosTabla();
        },err=>{
          this.procesarError(err.error)
        }); 
    }
  } 

  changeisUpdate=()=>{
    this.status="";
    this.idUpdating=NaN;
    this.registerUpdating=this.objInitial;
  }


  modalNuevo=()=>{
    this.status="creating";
    this.titulo="Crear de trámite";
    this.registerUpdating=this.objInitial;
  }


  procesarError=(error:any)=>{
    const msg = [] as string[];
    Object.values(error).forEach((val)=> {

      msg.push(val as string);
    });
    alert(msg.join("\n"))
  }
}
