import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { Empleado } from 'src/app/models/empleado';
import { Persona } from 'src/app/models/persona';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { NgForm } from '@angular/forms';
import { PersonaService } from 'src/app/services/persona.service';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent {
  constructor(private serivce:EmpleadoService,private serivceP:PersonaService){}
 
  dtOptions: DataTables.Settings = {};
  datos: any[]=  [];
  dtTrigger: Subject<any>= new Subject();
  status ="";
  idUpdating:number=NaN;
  registerUpdating:Empleado ={
    id: NaN,
    dependencia: "",
    fechaIngreso: "",
    empleadoId:NaN
  }
  objInitial:Empleado= {...this.registerUpdating};
  titulo:string="";

  personas:any;

  ngOnInit(): void {
    this.dtOptions = {  
      pageLength: 6,  
      stateSave:true,  
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],  
      processing: true  
    };  
    

    this.datosTabla();
    this.datosPersonas();

  }


  datosTabla=()=>{
    this.serivce.getAll().subscribe(response =>{  
      this.datos =response;  
      this.dtTrigger.next;
    }); 
  }

  datosPersonas=()=>{
    this.serivceP.getAll().subscribe(response =>{  
      this.personas =response;  
    }); 
  }
 

  modalActualizacion=(id:number)=>{
    this.status="editing";
    this.titulo="Actualización de Empleado";
    this.serivce.getOne(id)
    .subscribe((response) =>{
      this.idUpdating=id;
      this.registerUpdating= { 
            id: (response as any).id,
            dependencia: (response as any).dependencia,
            fechaIngreso: (response as any).fechaIngreso,
            empleadoId: (response as any).persona.id,
            persona:  ((response as any).persona as Persona), 
        }
    });  
  }
  registrar=(data:NgForm)=>{  
    
    if(this.status=="editing"){
        
        this.serivce.update(this.idUpdating,this.registerUpdating)
        .subscribe((response) => { 
          this.status="confirmado";
          this.datosTabla();
        },err=>{
          this.procesarError(err.error)
        });  
    }

    if(this.status=="creating"){

      this.serivceP.getOne(this.registerUpdating.empleadoId).subscribe(response =>{  
        this.registerUpdating.persona = (response as Persona);  

        this.serivce.create(this.registerUpdating)
        .subscribe((response) => { 
          this.status="confirmado";
          this.datosTabla();
        },err=>{
          this.procesarError(err.error)
        });  
      },err=>{
        this.procesarError(err.error)
      }); 


  }
  } 

  eliminar=(id:number)=>{  
    if(confirm("¿Realmente desea eliminar el registro")){
        this.serivce.delete(id)
        .subscribe((response) => { 
          this.status="";
          this.datosTabla();
        },err=>{
          this.procesarError(err.error)
        }); 
    }
  } 

  changeisUpdate=()=>{
    this.status="";
    this.idUpdating=NaN;
    this.registerUpdating=this.objInitial;
  }


  modalNuevo=()=>{
    this.status="creating";
    this.titulo="Crear de Empleado";
    this.registerUpdating=this.objInitial;
  }


  procesarError=(error:any)=>{
    const msg = [] as string[];
    Object.values(error).forEach((val)=> {

      msg.push(val as string);
    });
    alert(msg.join("\n"))
  }
}
