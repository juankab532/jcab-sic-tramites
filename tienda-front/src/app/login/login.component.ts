import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Apiservice } from '../api.service';
import { Credentials } from '../models/credentials';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(
    private apiService: Apiservice,
    private router: Router
  ){}
  creds: Credentials={
      email:"",
      password:""
  };
  mensaje:string="";



  login(form:NgForm){
    this.mensaje="";
    console.log("Form data", form.value);

    this.apiService.login(this.creds)
    .subscribe({
      next:(response)=>{
        //this.router.navigate(['/home']);
        document.location.href="/home";
      },
      error:(e)=>{
        if(e.status==401){
            this.mensaje="Datos de acceso incorrectos";
        }else{
          this.mensaje=e.message;
        }
      }
    });


  }



}
