import { Component } from '@angular/core';
import { Apiservice } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'demo';
  hasLogin:boolean=false;

  constructor( private apiService: Apiservice){}

  ngOnInit(): void {
    if(this.apiService.getToken()){
      this.hasLogin= true;
    }else{
      this.hasLogin= false;
    }
  }

  logout=()=>{
    this.apiService.logout();
  }


}
