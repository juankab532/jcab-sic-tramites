import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './helpers/auth.interceptor';
import { PersonaComponent } from './pages/persona/persona/persona.component';
import { EmpleadoComponent } from './pages/empleado/empleado/empleado.component';
import { TramiteComponent } from './pages/tramite/tramite/tramite.component';
import { ListaUsuariosComponent } from './pages/lsita-usuarios/lista-usuarios/lista-usuarios.component';
import { HomeComponent } from './pages/home/home/home.component';
import { DataTablesModule } from "angular-datatables";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PersonaComponent,
    EmpleadoComponent,
    TramiteComponent,
    ListaUsuariosComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    DataTablesModule
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass:AuthInterceptor,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
