import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Apiservice } from '../api.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private apiService: Apiservice) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let exceptions=["https://gorest.co.in/public/v1/users"];
    let currentUrl=request.url;

    if(exceptions.indexOf(currentUrl) !== -1) {
      return next.handle(request);
    }

    const token = this.apiService.getToken();
    console.log(request);
    if(token){
      const cloned = request.clone({
        headers:request.headers.set("Authorization",`Bearer ${token}`)
      })
      return next.handle(cloned);
    }

    return next.handle(request);
  }
}
