import { Injectable } from "@angular/core";
import {HttpClient, HttpResponse} from "@angular/common/http"
import { map, Observable } from "rxjs";
import { Credentials } from "./models/credentials";
import { Router } from "@angular/router";

@Injectable({
    providedIn:'root'
})

export class Apiservice{
    constructor(
        private http: HttpClient,
        private router:Router
    ){}

    login=(creds:Credentials)=>{
        return this.http.post("http://localhost:8000/login",creds,{
            observe:'response'
        }).pipe(map((response: HttpResponse<any>)=>{
            console.log(response);
            const body =response.body;
            const headers= response.headers;
            const bearerToken = headers.get("Authorization");
            
            if(bearerToken!=undefined){
                console.log("bearerToken "+bearerToken);
                const token = bearerToken.replace("Bearer ",'');
                localStorage.setItem("token",token);
            }else{
                localStorage.removeItem("token");
            }

            return body;
        }))
    }


    getToken(){
        return localStorage.getItem("token");
    }


    logout=()=>{
        localStorage.removeItem("token");
        //this.router.navigate(['login']);
        document.location.href="/login";
    }
}