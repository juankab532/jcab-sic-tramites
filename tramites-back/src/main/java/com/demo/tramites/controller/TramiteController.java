package com.demo.tramites.controller;

import com.demo.tramites.models.entities.Tramite;
import com.demo.tramites.services.TramiteService;
import com.demo.tramites.services.UtilsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/tramite")
@Log
@RequiredArgsConstructor
public class TramiteController {
    private final TramiteService service;
    private final UtilsService utils;

    @GetMapping("/")
    public ResponseEntity<?> list(){
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> detalle(@PathVariable Long id){
        Optional<Tramite> o= service.findById(id);
        if(o.isPresent()){
            return ResponseEntity.ok(o.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<?> crear(@Valid @RequestBody Tramite Tramite, BindingResult result){
        if(result.hasErrors()){
            return utils.validar(result);
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(Tramite));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editar(@Valid @RequestBody Tramite Tramite,BindingResult result,@PathVariable Long id) {
        Optional<Tramite> o = service.findById(id);
        if(o.isPresent()){
            if(result.hasErrors()){
                return utils.validar(result);
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(Tramite));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminar(@PathVariable Long id){
        Optional<Tramite> o = service.findById(id);
        if(o.isPresent()){
            service.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}
