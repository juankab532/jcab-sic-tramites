package com.demo.tramites.controller;

import com.demo.tramites.security.WebSecurityConfig;
import com.demo.tramites.models.entities.Persona;
import com.demo.tramites.services.PersonaService;
import com.demo.tramites.services.UtilsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/persona")
@Log
@RequiredArgsConstructor
public class PersonaController {
    private final PersonaService service;
    private final UtilsService utils;
    private final WebSecurityConfig webSecurityConfig;

    @GetMapping("/")
    public ResponseEntity<?> list(@PathVariable(required = false) Optional<Integer> page, @PathVariable(required = false) Optional<Integer>  size){
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> detalle(@PathVariable Long id){
        Optional<Persona> o= service.findById(id);
        if(o.isPresent()){
            return ResponseEntity.ok(o.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<?> crear(@Valid @RequestBody Persona persona, BindingResult result){
        if(result.hasErrors()){
            Map<String,String> errores= new HashMap<>();

            result.getFieldErrors().forEach(err ->{
                errores.put(err.getField(),err.getDefaultMessage());
            });

            if(persona.getPassword().isEmpty()) {
                errores.put("password","Proporcione una contraseña");
            }

            if(!persona.getPassword().isEmpty() && persona.getPassword().length()<8) {
                errores.put("password","La contraseña debe tener al menos 8 caracteres");
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errores);
            //return utils.validar(result);
        }
        if(!persona.getPassword().isEmpty()){
            persona.setPassword(webSecurityConfig.passwordEncoder().encode(persona.getPassword()));
        }else{
            Map<String,String> errores= new HashMap<>();
            errores.put("password","Proporcione una contraseña");

            if(persona.getPassword().length()<8) {
                errores.put("password","La contraseña debe tener al menos 8 caracteres");
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errores);
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(persona));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editar(@Valid @RequestBody Persona persona,BindingResult result,@PathVariable Long id) {
        Optional<Persona> o = service.findById(id);
        if(o.isPresent()){
            if(result.hasErrors()){
                return utils.validar(result);
            }
            if(!persona.getPassword().isEmpty()){
                persona.setPassword(webSecurityConfig.passwordEncoder().encode(persona.getPassword()));
            }else{
                persona.setPassword(o.get().getPassword());
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(persona));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminar(@PathVariable Long id){
        Optional<Persona> o = service.findById(id);
        if(o.isPresent()){
            service.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}
