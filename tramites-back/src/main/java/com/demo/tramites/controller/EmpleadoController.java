package com.demo.tramites.controller;

import com.demo.tramites.security.WebSecurityConfig;
import com.demo.tramites.models.entities.Empleado;
import com.demo.tramites.services.EmpeladoService;
import com.demo.tramites.services.UtilsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/empleado")
@Log
@RequiredArgsConstructor
public class EmpleadoController {
    private final EmpeladoService service;
    private final UtilsService utils;
    private final WebSecurityConfig webSecurityConfig;

    @GetMapping("/")
    public ResponseEntity<?> list(){
        return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> detalle(@PathVariable Long id){
        Optional<Empleado> o= service.findById(id);
        if(o.isPresent()){
            return ResponseEntity.ok(o.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<?> crear(@Valid @RequestBody Empleado empleado, BindingResult result){
        if(result.hasErrors()){
            return utils.validar(result);
        }
        if(!empleado.getPersona().getPassword().isEmpty()){
            empleado.getPersona().setPassword(webSecurityConfig.passwordEncoder().encode(empleado.getPersona().getPassword()));
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(empleado));
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editar(@Valid @RequestBody Empleado empleado,BindingResult result,@PathVariable Long id) {
        Optional<Empleado> o = service.findById(id);
        if(o.isPresent()){
            if(result.hasErrors()){
                return utils.validar(result);
            }
            if(!empleado.getPersona().getPassword().isEmpty()){
                empleado.getPersona().setPassword(webSecurityConfig.passwordEncoder().encode(empleado.getPersona().getPassword()));
            }
            return ResponseEntity.status(HttpStatus.CREATED).body(service.save(empleado));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminar(@PathVariable Long id){
        Optional<Empleado> o = service.findById(id);
        if(o.isPresent()){
            service.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}
