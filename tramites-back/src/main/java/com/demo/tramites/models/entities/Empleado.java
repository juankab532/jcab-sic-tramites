package com.demo.tramites.models.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity(name = "empleados")
public class Empleado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Size(min=5 , max = 255,message = "La dependencia debe tener entre 5 y 255 caractéres")
    @NotBlank(message = "Debe ingresar la dependencia")
    String dependencia;

    @Temporal(TemporalType.DATE)
    @NotNull(message = "Debe proporcionar una fecha de ingreso")
    Date fechaIngreso;

    @OneToOne
    Persona persona;


}
