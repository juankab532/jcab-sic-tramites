package com.demo.tramites.models.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;

@Data
@Entity(name = "personas")
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;


    @NotEmpty(message = "Debe proporcionar un tipo de documento")
    String tipoDocumento;

    @Size(min=5 , max = 255,message = "El dpcumento debe tener entre 5 y 255 caractéres")
    @NotNull(message = "Debe ingrasar un documento")
    String documento;

    @Size(min=5 , max = 255,message = "El nombre debe tener entre 5 y 255 caractéres")
    @NotBlank(message = "Debe ingresar el nombre")
    String nombres;

    @Size(min=5 , max = 255,message = "Los apellidos debe tener entre 5 y 255 caractéres")
    @NotBlank (message = "Debe ingresar los apellidos una dirección")
    String apellidos;

    @Size(min=7 , max = 10,message = "El teléfono debe tener entre 7 y 10 caractéres")
    @Column(length = 10)
    String telefono;

    @Size(min=5 , max = 255,message = "La dirección debe tener entre 5 y 255 caractéres")
    @NotBlank(message = "Debe ingresar una dirección")
    String direccion;

    @Email(message = "Proporcione un correo válido")
    @Column(unique=true)
    String email;

    //@Size(min=8,max = 20,message = "La contraseña debe tener entre 8 y 20 carateres")
    //@NotEmpty(message = "Proporcione una contraseña")
    String password;


}


