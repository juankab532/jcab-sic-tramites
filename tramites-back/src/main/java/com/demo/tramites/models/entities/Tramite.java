package com.demo.tramites.models.entities;

import lombok.Data;



import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Data
@Entity(name = "tramites")
public class Tramite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull(message = "Debe proporcionar un año")
    Integer anio;

    @Size(min=5 , max = 255)
    @NotBlank(message = "Especifique un nombre para el ´trámite")
    String nombre;

    @Size(min=5 ,max = 2000)
    @NotBlank(message = "Especifique una descripcion para el ´trámite")
    @Column(length = 2000)
    String descripcion;

    @ManyToOne
    Persona persona;

    @ManyToOne
    Empleado empleado;
}
