package com.demo.tramites.services;


import com.demo.tramites.models.entities.Persona;
import com.demo.tramites.repository.PersonaRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonaServiceImpl implements PersonaService{
    private final PersonaRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Persona> findAll() {
        return (List<Persona>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Persona> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Optional<Persona> findByEmail(String email) {
        return  repository.findOneByEmail(email);
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Persona save(Persona Tramite) {
        return repository.save(Tramite);
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
