package com.demo.tramites.services;

import com.demo.tramites.models.entities.Empleado;

import java.util.List;
import java.util.Optional;

public interface EmpeladoService {
    List<Empleado> findAll();
    Optional<Empleado> findById(Long id);
    Empleado save(Empleado tramite);
    void deleteById(Long id);
}
