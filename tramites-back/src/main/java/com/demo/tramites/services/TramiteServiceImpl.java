package com.demo.tramites.services;

import com.demo.tramites.models.entities.Tramite;
import com.demo.tramites.repository.TramiteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TramiteServiceImpl implements TramiteService{
    private final TramiteRepository repository;

    @Override
    @Transactional(readOnly = true)
    public List<Tramite> findAll() {
        return (List<Tramite>) repository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Tramite> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Tramite save(Tramite Tramite) {
        return repository.save(Tramite);
    }

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
