package com.demo.tramites.services;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import java.util.Map;

public interface UtilsService {
    ResponseEntity<Map<String,String>> validar(BindingResult result);
}
