package com.demo.tramites.services;

import com.demo.tramites.models.entities.Persona;


import java.util.List;
import java.util.Optional;

public interface PersonaService {
    List<Persona> findAll();
    Optional<Persona> findById(Long id);
    Optional<Persona> findByEmail(String email);
    Persona save(Persona tramite);
    void deleteById(Long id);
}
