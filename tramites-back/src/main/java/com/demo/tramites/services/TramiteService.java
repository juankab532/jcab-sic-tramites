package com.demo.tramites.services;

import com.demo.tramites.models.entities.Tramite;

import java.util.List;
import java.util.Optional;

public interface TramiteService {
    List<Tramite> findAll();
    Optional<Tramite> findById(Long id);
    Tramite save(Tramite tramite);
    void deleteById(Long id);
}
