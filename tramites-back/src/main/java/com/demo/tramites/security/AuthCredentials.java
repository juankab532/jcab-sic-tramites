package com.demo.tramites.security;

import lombok.Data;


@Data
public class AuthCredentials {
    private String email;
    private String password;
}
