package com.demo.tramites.security;

import com.demo.tramites.models.entities.Persona;
import com.demo.tramites.services.PersonaService;
import lombok.RequiredArgsConstructor;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserDatilServiceImpl implements UserDetailsService {
    private final PersonaService personaRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Persona usuario=personaRepository
                .findByEmail(username)
                .orElseThrow(()-> new UsernameNotFoundException("El usuario con email: "+username+" no existe."));
        UserDetailsImpl userDetails=new UserDetailsImpl(usuario);
        return userDetails;
    }
}
