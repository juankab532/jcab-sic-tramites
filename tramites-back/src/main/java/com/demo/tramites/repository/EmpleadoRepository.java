package com.demo.tramites.repository;

import com.demo.tramites.models.entities.Empleado;
import org.springframework.data.repository.CrudRepository;
public interface EmpleadoRepository extends CrudRepository<Empleado,Long> {
}
