package com.demo.tramites.repository;

import com.demo.tramites.models.entities.Tramite;
import org.springframework.data.repository.CrudRepository;

public interface TramiteRepository extends CrudRepository<Tramite,Long> {
}
