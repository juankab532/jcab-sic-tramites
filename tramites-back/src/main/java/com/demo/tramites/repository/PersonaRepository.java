package com.demo.tramites.repository;

import com.demo.tramites.models.entities.Persona;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PersonaRepository extends CrudRepository<Persona,Long> {
    Optional<Persona> findOneByEmail(String email);
}
